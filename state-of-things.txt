// NOTES
Added checks for all functions in style.js
Fixed smooth scrolling on scroll to arrow/link
Streched waves background for to acommdate larger screens
Solved issue with accordion on product-pricing page
Fixed logo 
Both mobile and main menu

// QUESTIONS
Should there be charts on the site?

// FINISHED PAGES
about.html
audit-management.html
contact.html
controls.html
corporate-license.html
events-calendar.html
index.html
it-tracking.html
learning.html
partner.html - which image to add? 
product-pricing.html
request-demo.html
service.html
solution.html
support.html
system-status.html - which image to add? are you aware that prev, next button don't work?
training.html
use-case.html - which image to add?


// 2ND TIER
Red Button on bottom right needs to be aligned little bit up. - done
Contact page. Northwest Mutual, UCON Icon banner needs to be updated. Rest of Icons need to be adjusted to other businesses and one line needs to be placed off. - not done
JS contact page/request demo page to send information to user. - not clear what needs to happen. Do you mean to send mail to the address? We need php for that.
TOS https://www.qasymphony.com/terms-of-service/ - I need create privacy page and fill with this content
Privacy. https://www.qasymphony.com/privacy-policy/ - I need create privacy page and fill with this content?
