window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  if($('#toTop').length) {
    if (document.body.scrollTop > 1000 || document.documentElement.scrollTop > 1000) {
      document.getElementById("toTop").style.visibility = "visible";
    } else {
      document.getElementById("toTop").style.visibility = "hidden";
    }
  }
}

//

$(window).scroll(function () {
  if ($(this).scrollTop() > 100) {
    $('.scrollup').fadeIn();
  } else {
    $('.scrollup').fadeOut();
  }
});

$('.scrollup').click(function () {
  $("html, body").animate({
    scrollTop: 0
  }, 1000);
  return false;
});

if($('#fixnav').length) {
  var elementPosition = $('#fixnav').offset();
  $(window).scroll(function () {
    if ($(window).scrollTop() > elementPosition.top) {
      $('#fixnav').css('position', 'fixed').css('top', '0');
    } else {
      $('#fixnav').css('position', 'relative');
    }
  });
}

$(document).ready(function () {

  if($(".main-menu ul#menu").length) {
    $(".main-menu ul#menu").slicknav({
      allowParentLinks: true,
      prependTo: '.mobile-menu',
      label: ''
    });
  }

  // Add smooth scrolling to all links
  // $("a.link-scroll").on('click', function (event) {
  //   if (this.hash !== "") {
  // 
  //     event.preventDefault();
  //     var hash = this.hash;
  // 
  //     // Using jQuery's animate() method to add smooth page scroll
  //     // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
  //     $('html, body').animate({
  //       scrollTop: $(hash).offset().top
  //     }, 700, function () {
  // 
  //       // Add hash (#) to URL when done scrolling (default click behavior)
  //       window.location.hash = hash;
  //     });
  //   } // End if
  // });


  // scroll down

  $(".down, .link-scroll").click(function (e) {
    e.preventDefault();
    var clickedElementHref = $(this).prop('href');
    var targetElement = clickedElementHref.substring(clickedElementHref.indexOf('#'));
    $('html, body').animate({
      scrollTop: $(targetElement).offset().top
    }, 1000);
  });
  


  // code for tab

  $('.tab-button').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });



  // $('.tab-button').on('click', function(){
  //     $('.tab-button span.underline').removeClass('underline');
  //     $('.tab-button span').addClass('underline');
  // });

  $('.tab-button').on('click', function (e) {
    e.preventDefault();
    $('.tab-button.current').removeClass('current');
    $(this).addClass('current');
  });





  // events-calendar

  $(".table-group-wrapper .single-group").each(function (e) {
    if (e != 0)
      $(this).hide();
  });

  $("#next").click(function () {
    if ($(".table-group-wrapper .single-group:visible").next().length != 0)
      $(".table-group-wrapper .single-group:visible").next().show().prev().hide();
    else {
      $(".table-group-wrapper .single-group:visible").hide();
      $(".table-group-wrapper .single-group:first").show();
    }
    return false;
  });

  $("#prev").click(function () {
    if ($(".table-group-wrapper .single-group:visible").prev().length != 0)
      $(".table-group-wrapper .single-group:visible").prev().show().next().hide();
    else {
      $(".table-group-wrapper .single-group:visible").hide();
      $(".table-group-wrapper .single-group:last").show();
    }
    return false;
  });

  // month switcher  
  $('.js-month-wrapper').find('a').each(function(){
    // when clicked on month switcher link
    $(this).on('click', function(){
      // get its hash
      var targetEl = this.hash.substr(1);
      // go through all single groups
      $('.table-group-wrapper .single-group').each(function(){
        // check if current has month class
        if($(this).hasClass(targetEl)) {
          // and show it if it has
          $(this).show();
        }
        else {
          // hide it if it doesn't
          $(this).hide();
        }
      });
    });
  });

  // Testimonial Slider
  if($(".testimonial-slider-wrapper").length) {
    $(".testimonial-slider-wrapper").owlCarousel({
      items: 1,
      nav: true,
      dots: false,
      autoplay: false,
      loop: true,
      navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
      mouseDrag: false,
      touchDrag: false
    });
  }

  // countdown
  if($('.counter').length) {
    $('.counter').counterUp({
      delay: 10,
      time: 1000
    });
  }


  // collapse-icon-change

  $('.collapse').on('shown.bs.collapse', function () {
    $(this).parent().find('.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');
    $(this).parent().find('.fa-plus').removeClass('fa-plus').addClass('fa-minus');
  }).on('hidden.bs.collapse', function () {
    $(this).parent().find('.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down');
    $(this).parent().find('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
  });



});

// if($('#chartHero').length) {
//   var ctx = document.getElementById("chartHero").getContext('2d');
//   var myChart = new Chart(ctx, {
//     type: 'line',
//     data: {
//       labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
//       datasets: [{
//         showLines: false,
//         label: '',
//         data: [13, 18, 23, 16, 32, 38],
//         backgroundColor: [
//           'red'
//         ],
//         borderColor: [
//           'yell'
//         ],
//         stepped: false
//       }]
//     },
//     options: {
//       scales: {
//         yAxes: [{
//           ticks: {
//             beginAtZero: true
//           }
//         }]
//       },
//       showLines: true,
//       borderColor: '0'
//     }
//   });
// }
