//
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 1000 || document.documentElement.scrollTop > 1000) {
        document.getElementById("toTop").style.visibility = "visible";
    } else {
        document.getElementById("toTop").style.visibility = "hidden";
    }
}

//

var elementPosition = $('#fixnav').offset();

$(window).scroll(function(){
        if($(window).scrollTop() > elementPosition.top){
              $('#fixnav').css('position','fixed').css('top','0');
        } else {
            $('#fixnav').css('position','relative');
        }
});
